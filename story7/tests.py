from django.test import TestCase, Client
from django.urls import resolve
from .views import story7

# Create your tests here.

class Story7Test(TestCase):
    def test_story_7_is_exist(self):
        response = Client().get('/story7/')
        self.assertEqual(response.status_code, 200)
        
    def test_story_7_using_index_func(self):
        found = resolve('/story7/')
        self.assertEqual(found.func, story7)
    
    def test_story7_using_index_template(self):
        response = Client().get('/story7/')
        self.assertTemplateUsed(response, '/story7.html')
