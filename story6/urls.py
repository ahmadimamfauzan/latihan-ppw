from django.urls import path

from . import views

app_name = 'story6'

urlpatterns = [
    path('register/', views.daftar, name='daftar'),
    path('delete/', views.hapus, name="hapus"),
    path('activity-list/', views.Utama, name='kegiatan_list'),
]
