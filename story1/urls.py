from django.urls import path

from . import views

app_name = 'story1'

urlpatterns = [
    path('story-1/', views.story1, name='story-1')
]