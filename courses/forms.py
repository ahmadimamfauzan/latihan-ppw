from django import forms

from .models import MyCourse

class CourseForm(forms.ModelForm):
    name = forms.CharField()
    lecture = forms.CharField()
    sks = forms.IntegerField()
    term = forms.IntegerField
    room = forms.CharField()
    desc = forms.CharField()

    class Meta:
        model = MyCourse
        fields = '__all__'
