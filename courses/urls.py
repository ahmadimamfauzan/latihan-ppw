from django.urls import path

from . import views

app_name = 'courses'

urlpatterns = [
    path('courses-list/', views.coursesList, name='courses-list'),
    path('courses-list/<int:id>', views.courseDetail),
    path('delete-courses/<int:id>', views.deleteCourse, name="delete-course"),
    path('add-course', views.addCourse, name='add-course'),
    path('update-course/<int:id>', views.updateCourse, name='update-course')
]
