from django.shortcuts import render, redirect
from .models import MyCourse
from .forms import CourseForm

# Create your views here.

def coursesList(request):
    courses = MyCourse.objects.all()
    return render(request, 'courses-list.html', {'courses':courses})

def courseDetail(request, id):
    courses = MyCourse.objects.get(id=id)
    return render(request, 'course-detail.html', {'courses':courses})

def deleteCourse(request, id):
    course = MyCourse.objects.get(id=id)
    if request.method == 'POST':
        course.delete()
        return redirect('/courses-list')
    return render(request, 'delete-course.html', {'course': course})

def addCourse(request):
    form = CourseForm()
    if request.method == 'POST':
        form = CourseForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/courses-list')
    return render(request, 'course-form.html', {'form':form})

def updateCourse(request, id):
    course = MyCourse.objects.get(id=id)
    form = CourseForm(instance=course)
    if request.method == 'POST':
        form = CourseForm(request.POST, instance=course)
        if form.is_valid():
            form.save()
            return redirect('/courses-list')
    return render(request, 'course-form.html', {'form':form})