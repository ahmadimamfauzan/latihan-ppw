from django.db import models

# Create your models here.

class MyCourse(models.Model):
    name = models.CharField(max_length = 100)
    lecture = models.CharField(max_length = 100)
    sks = models.PositiveIntegerField()
    term = models.PositiveIntegerField()
    room = models.CharField(max_length= 100)
    desc = models.CharField(max_length = 500)

    def __str__(self):
        return self.name
