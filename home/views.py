from django.shortcuts import render

# Create your views here.

def index(request):
    return render(request, 'home/index3.html')

def about(request):
    return render(request, 'home/about.html')

def experiences(request):
    return render(request, 'home/experiences.html')

def portfolio(request):
    return render(request, 'home/portfolio.html')