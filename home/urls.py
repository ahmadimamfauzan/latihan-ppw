from django.urls import path

from . import views

app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('portfolio/', views.portfolio, name='portfolio'),
    path('experiences/', views.experiences, name='experiences'),
    path('about/', views.about, name='about')
]